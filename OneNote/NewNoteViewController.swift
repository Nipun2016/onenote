//
//  NewNoteViewController.swift
//  OneNote
//
//  Created by BS23 on 5/21/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class NewNoteViewController: UIViewController {
    
     var ref : FIRDatabaseReference?
    
    @IBOutlet weak var headingTextField: UITextField!
    
    @IBOutlet weak var contantUiView: UIView!
    @IBOutlet weak var datePickerTextField: UITextField!
    var datePacker = UIDatePicker()
    
    @IBOutlet weak var textField: UITextView!
    
    //test commit
    
    func createDatePacker ()
    {
        //Format of date picker
        
        datePacker.datePickerMode = .date
        
        //toolBar
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        //Bar Button Item
        let doneButton = UIBarButtonItem (barButtonSystemItem: .done, target: nil, action: #selector (donePressed))
        toolBar.setItems([doneButton], animated: false)
        
        datePickerTextField.inputAccessoryView = toolBar
        datePickerTextField.inputView = datePacker
   
    }
    
    func donePressed()
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        datePickerTextField.text = dateFormatter.string(from: datePacker.date)
        self.view.endEditing(true)
    }

    
    
    @IBAction func addButtonClicked(_ sender: Any)
    {
       
        let post = ["Date": datePickerTextField.text,
            "Heading": headingTextField.text,
            "Post": textField.text]
        
        ref?.child("Notes").childByAutoId().setValue(post)
        
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func discardButtonPressed(_ sender: Any)
    {
         self.navigationController?.popViewController(animated: true)
    }
    
    
    func titleName()
    {
        self.title = "Compose"
        
        let attributes = [NSFontAttributeName: UIFont(name: "Zapfino", size: 17)!, NSForegroundColorAttributeName: UIColor.purple];
        self.navigationController?.navigationBar.titleTextAttributes = attributes
   
    }
        
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ref =  FIRDatabase.database().reference()
        
        
        createDatePacker()
        titleName()
        
        
    }

 
}
