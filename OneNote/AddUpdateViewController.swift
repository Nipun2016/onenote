//
//  AddUpdateViewController.swift
//  OneNote
//
//  Created by BS23 on 5/21/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class AddUpdateViewController: UIViewController {
    
    var key:String = ""
    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var ref :  FIRDatabaseReference?
    var databaseHandler : FIRDatabaseHandle?
    
    
    
   
    @IBAction func deleteButtonClick(_ sender: Any)
    {
        let ref = FIRDatabase.database().reference().child("Notes")
        
        ref.child(key).removeValue
            { (error, ref) in
                if error != nil
                {
                    print("error")
                }
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func updateButtonClick(_ sender: Any)
    {
        
        let ref = FIRDatabase.database().reference().child("Notes")

        
        let post = ["Date": date.text,
                    "Heading": heading.text,
                    "Post": textView.text] //as [String : Any]
        
        let childUpdates = ["/\(key)": post]

        ref.updateChildValues(childUpdates) { (error, frReference) in
            if error != nil{
                
            }
            else{
                self.navigationController?.popViewController(animated: true)
                
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
    }
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        print(key)
        
        ref = FIRDatabase.database().reference()
        ref?.child("Notes").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            self.heading.text = value?["Heading"] as? String ?? ""
            self.date.text = value?["Date"] as? String ?? ""
            self.textView.text = value?["Post"] as? String ?? ""
            
        }) { (error) in
            print(error.localizedDescription)
        }

    }
   
}
