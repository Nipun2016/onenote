//
//  HomeViewController.swift
//  OneNote
//
//  Created by BS23 on 5/15/17.
//  Copyright © 2017 edu. All rights reserved.
//

import UIKit
import Firebase


class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var NotesList = [Notes]()
    var ref :  FIRDatabaseReference?  //Keep the reference of the path
    var databaseHandler : FIRDatabaseHandle?
    var key:String = ""
    var objectKey = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    
   
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return NotesList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomeTableViewCell
        
        Cell.heading.text =  NotesList[indexPath.row].heading
        Cell.date.text = NotesList[indexPath.row].date
        Cell.textField.text = NotesList[indexPath.row].post
        
        
        return Cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        key = objectKey[indexPath.row]
        print("Key")
        print(key)
        
        self.performSegue(withIdentifier: "AddUpdateSegue", sender: self)
    }
    
    
    
   /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    } */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "AddUpdateSegue"
        {
            let destinationViewController = segue.destination as! AddUpdateViewController
            destinationViewController.key = self.key
        }
    }
    
    
    
    
    func fetchNotes()
    {
        NotesList = NSMutableArray() as! [Notes]
        key = ""
        objectKey = [String]()
        
        databaseHandler = ref?.child("Notes").observe(.childAdded, with: { (snapShoot) in
            let dictionary = snapShoot.value as? [String : AnyObject]
            let notes = Notes()
            
            
            notes.setValuesForKeys(dictionary!)
            self.NotesList.append(notes)
            self.objectKey.append(snapShoot.key)
            
            self.tableView.reloadData()
        })
    }
    

    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        ref = FIRDatabase.database().reference()
        
        self.tableView.estimatedRowHeight = 106.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        fetchNotes()
    }
}
